<?php

/**
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */
include_once(dirname(__FILE__) . '/../../mjtanexpert.php');

class MjtanexpertInvoiceModuleFrontController extends ModuleFrontController
{

    public $_html;
    public $prefix;
    public $display_column_left = false;
    public $auth = true;
    public $authRedirection = true;

    public function __construct()
    {

        $this->prefix = 'mjtanexpert_';
        $this->name = 'mjtanexpert';
        $this->bootstrap = true;
        parent::__construct();
    }

    public function init()
    {
        parent::init();
    }

    /**
     * Generowanie contentu dla faktur
     */
    public function initContent()
    {
        parent::initContent();
        $invoices = (new TanexpertCustomer())->getInvoices($this->context->customer->id);
        $szkoleniowcy = TanexpertCustomer::getCustomersFromExpert($this->context->customer->id);
        Context::getContext()->smarty->assign(array(
            'szkoleniowcy' => $szkoleniowcy,
            'invoices' => $invoices
        ));


        $this->setTemplate("module:mjtanexpert/views/templates/front/invoice.tpl");
    }

    /**
     * Wysyłka faktury
     */
    public function postProcess()
    {
        parent::postProcess();
        if (Tools::isSubmit('sendInvoice')) {

            if (!empty(Tools::getValue('prowizja'))) {

                if (isset($_FILES['invoice']) && isset($_FILES['invoice']['tmp_name']) && !empty($_FILES['invoice']['tmp_name'])) {
                    if (($_FILES['invoice']['size']) > 2000000) {

                        $this->context->smarty->assign("error", $this->module->l('Błąd podczas wysyłania faktury, maksymalny rozmiar pliku 2MB'));
                    } else {
                        $ext = Tools::substr($_FILES['invoice']['name'], strrpos($_FILES['invoice']['name'], '.') + 1);
                        if ($ext == 'pdf') {
                            $file_name = md5($_FILES['invoice']['name'] . time()) . '.' . $ext;

                            if (!move_uploaded_file($_FILES['invoice']['tmp_name'], dirname(__FILE__) . '/uploads/invoices/' . $file_name)) {
                                // return $this->displayError($this->module->l('An error occurred while attempting to upload the file.'));
                                $this->context->smarty->assign("error", $this->module->l('Błąd podczas wysyłania faktury') . " " . $_FILES['mediaphotos_photo']['name']);
                            } else {

                                $addInvoice = (new TanexpertCustomer())->addInvoice($this->context->customer->id, $file_name, Tools::getValue('prowizja'));
//                                        $addInspiration = 'INSERT INTO ' . _DB_PREFIX_ . 'social_inspirations (`email`,`photo`,`social_url`) VALUES ("' . pSQL($this->context->customer->email) . '","' . pSQL($file_name) . '","' . pSQL($_POST['mediaphotos_socialurl']) . '")';
//                                        DB::getInstance()->Execute($addInspiration, 1, 0);


                                $this->context->smarty->assign("success", $this->module->l('Faktura wysłana poprawnie'));

                                $this->sendMail($this->context->customer->email, $file_name);
                            }
                        } else {
                            $this->context->smarty->assign("error", $this->module->l('Niepopwany format pliku. Dozwolony są tylko pliki PDF'));
                        }
                    }
                } else {
                    $this->context->smarty->assign("error", $this->module->l('Prześlij fakturę'));
                }
            } else {
                $this->context->smarty->assign("error", $this->module->l('Podaj wartość prowizji'));
            }
        }
    }

    /**
     * Wysyłka emaili
     * @param type $email
     * @param type $invoice
     */
    public function sendMail($email, $invoice)
    {

        $customer = (new Customer())->getCustomersByEmail($email);

        $content = '<h3>' . $this->module->l('Faktura od klienta') . '</h3>';
        $content .= '<h2>' . $customer[0]['firstname'] . ' ' . $customer[0]['lastname'] . '</h2>';
        ;


        $content .= '<br/>' . $this->module->l('Faktura') . ':<br/> ';
        $content .= '<a href="http://' . (Tools::getShopDomain(false, false)) . '/modules/' . $this->name . '/controllers/front/uploads/invoices/' . $invoice . '">http://' . (Tools::getShopDomain(false, false)) . '/modules/' . $this->name . '/controllers/front/uploads/invoices/' . $invoice . '</a><br/>';

        if (Validate::isEmail(Configuration::get('PS_SHOP_EMAIL'))) {
            Mail::Send(
                    Configuration::get('PS_LANG_DEFAULT'), // id lang
                    'contact', // template 
                    'Faktura od ' . $customer[0]['firstname'] . ' ' . $customer[0]['lastname'] . '', // subject
                    array(
                '{email}' => Configuration::get('PS_SHOP_EMAIL'), // sender email address
                '{message}' => $content, // template vars
                '{order_name}' => '',
                '{attached_file}' => ''
                    ), Configuration::get('PS_SHOP_EMAIL'), // to //
                    null, //$to_name = 
                    null, //$from = 
                    null, //$from_name = 
                    null, //$file_attachment = 
                    null, //$mode_smtp = 
                    _PS_MAIL_DIR_, //$template_path = 
                    false, //$die = 
                    null, //$id_shop
                    null, //$bcc
                    null // reply_to
            );
        }
    }

}
