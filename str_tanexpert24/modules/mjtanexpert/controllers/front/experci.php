<?php
/**
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

include_once(dirname(__FILE__).'/../../mjtanexpert.php');

class MjtanexpertExperciModuleFrontController extends ModuleFrontController
{
    public $_html;
    public  $prefix;
    
        public $display_column_left = false;
        public $auth = true;
        public $authRedirection = true;
        
    public function __construct()
    {

        $this->prefix = 'mjtanexpert_';
        $this->name = 'mjtanexpert';
        $this->bootstrap = true;
        parent::__construct();
    }
    public function postProcess()
    {
        parent::postProcess();
        
    }
    public function init() {
     parent::init();
    }
    public function initContent()
    {
     parent::initContent();
     
    $szkoleniowcy = TanexpertCustomer::getCustomersFromExpert($this->context->customer->id);
        //$this->setTemplate('experci.tpl');
            Context::getContext()->smarty->assign(array(
            'szkoleniowcy' => $szkoleniowcy,
        ));

         
     $this->setTemplate("module:mjtanexpert/views/templates/front/experci.tpl");
     }
}
