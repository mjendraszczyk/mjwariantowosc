<?php

/**
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */
include_once(dirname(__FILE__) . '/../../mjtanexpert.php');

class MjtanexpertSaldoModuleFrontController extends ModuleFrontController
{

    public $_html;
    public $prefix;
    public $display_column_left = false;
    public $auth = true;
    public $authRedirection = true;

    public function __construct()
    {

        $this->prefix = 'mjtanexpert_';
        $this->name = 'mjtanexpert';
        $this->bootstrap = true;
        parent::__construct();
    }

    public function postProcess()
    {
        parent::postProcess();
    }

    public function init()
    {
        parent::init();
    }

    public function initContent()
    {
        parent::initContent();


        //$this->setTemplate('saldo.tpl');

        //$query = 'SELECT * FROM ' . _DB_PREFIX_ . 'tanexpert_saldo ts LEFT JOIN ' . _DB_PREFIX_ . 'customer c ON ts.id_customer_klient = c.id_customer WHERE ts.id_customer_expert = "'.$this->context->customer->id.'"';

//          public static function getSpecificPrice(
//        $id_product,
//        $id_shop,
//        $id_currency,
//        $id_country,
//        $id_group,
//        $quantity,
//        $id_product_attribute = null,
//        $id_customer = 0,
//        $id_cart = 0,
//        $real_quantity = 0
//    )
        $query = 'SELECT * FROM '._DB_PREFIX_.'tanexpert_saldo ts LEFT JOIN '._DB_PREFIX_.'tanexpert_szkoleniowcy te ON te.id_customer_klient = ts.id_customer_klient LEFT JOIN '._DB_PREFIX_.'customer c ON ts.id_customer_klient = c.id_customer WHERE te.id_customer_expert = "'.$this->context->customer->id.'"';
        $saldo = DB::getInstance()->ExecuteS($query,1,0);
        $orders_detail = array();
        foreach ($saldo as $key => $order) {
            $orders_detail[$key]['reference'] = (new Order($order['id_order']))->reference;
            $orders_detail[$key]['value'] = (new Order($order['id_order']))->total_paid;
            $orders_detail[$key]['email'] = $order['email'];
            $orders_detail[$key]['saldo'] = $order['saldo'];
            $orders_detail[$key]['data_zamowienia'] = TanexpertCustomer::getOrderDate($saldo[$key]['id_order']);
            $orders_detail[$key]['id_order'] = $order['id_order'];
            $orders_detail[$key]['products'] = (new Order($order['id_order']))->getProducts();
        }
        $this->context->smarty->assign(array(
//            'salda' => $saldo,
            //'prowizja' => $getSpecialPrice,
            'orders_detail' => $orders_detail,
            'test' => 'tggg'
        ));

        $this->setTemplate("module:mjtanexpert/views/templates/front/saldo.tpl");
    }

}
