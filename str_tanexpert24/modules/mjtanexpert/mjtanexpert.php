<?php

/**
 * Main class of module Tanexpert
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */
require_once dirname(__FILE__) . '/classes/TanexpertCustomer.php';

class Mjtanexpert extends Module
{
    public $module;
    public $prefix;

    //  Inicjalizacja
    public function __construct()
    {
        $this->prefix = 'mjtanexpert_';

        $this->name = 'mjtanexpert';
        $this->tab = 'market_place';
        $this->version = '1.00';
        $this->author = 'MAGES Michał Jendraszczyk';
        $this->module_key = '3b0e2952aa2d9c3f87813b578f77506e';

        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Tanexpert - program afiliacyjny');
        $this->description = $this->l('Moduł pozwalający na zarządzanie klientami w programie afiliacyjnym');

        $this->confirmUninstall = $this->l('Remove module?');
    }

    private function installModuleTab($tabClass, $tabName, $idTabParent)
    {
        $tab = new Tab();
        $tab->name = $tabName;
        $tab->class_name = $tabClass;
        $tab->module = $this->name;
        $tab->id_parent = $idTabParent;
        $tab->position = 99;
        if (!$tab->save())
            return false;
        return true;
    }

    public function uninstallModuleTab($tabClass)
    {
        $idTab = Tab::getIdFromClassName($tabClass);
        if ($idTab) {
            $tab = new Tab($idTab);
            $tab->delete();
        }
        return true;
    }

    //  Instalacja
    public function install()
    {
        parent::install() && $this->installModuleTab('AdminTanexpertconfiguration', array(Configuration::get('PS_LANG_DEFAULT') => 'Tanexpert program afiliacyjny'), Tab::getIdFromClassName('AdminParentOrders')) && $this->installModuleTab('AdminTanexpertcustomer', array(Configuration::get('PS_LANG_DEFAULT') => 'Tanexpert klienci'), Tab::getIdFromClassName('AdminParentOrders')) && $this->registerHook('actionValidateOrder') && $this->registerHook('displayCustomerAccount') && $this->registerHook('displayAdminCustomers') && $this->registerHook('actionOrderStatusPostUpdate') && $this->registerHook('actionObjectUpdateAfter');

        $createTableExpert = 'CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . 'tanexpert_szkoleniowcy (`id` INT(16) NOT NULL AUTO_INCREMENT ,`id_customer_expert` INT(16) NOT NULL, `id_customer_klient` INT(16) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1';
        DB::getInstance()->Execute($createTableExpert, 1, 0);

        $createTableSaldo = 'CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . 'tanexpert_saldo (`id` INT(16) NOT NULL AUTO_INCREMENT ,`id_order` INT(16) NOT NULL, `id_customer_klient` INT(16) NOT NULL, `saldo` FLOAT(16) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1';
        DB::getInstance()->Execute($createTableSaldo, 1, 0);

        $createTableFaktury = 'CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . 'tanexpert_faktury (`id_tanexpert_faktury` INT(16) NOT NULL AUTO_INCREMENT ,`prowizja` FLOAT(16) NOT NULL, `id_customer_expert` INT(16) NOT NULL, `faktura` VARCHAR(255) NULL,`data_dodania` DATE NOT NULL , PRIMARY KEY (`id_tanexpert_faktury`)) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1';
        DB::getInstance()->Execute($createTableFaktury, 1, 0);

//        Configuration::updateValue($this->prefix . 'expert_group', '');
//        Configuration::updateValue($this->prefix . 'expert_szkoleniowiec', '');
//        Configuration::updateValue($this->prefix . 'expert_klient', '');

        return true;
    }

    // Deinstalacja
    public function uninstall()
    {
        return parent::uninstall() && $this->uninstallModuleTab('AdminTanexpertconfiguration') && $this->uninstallModuleTab('AdminTanexpertcustomer');
    }

    public function hookActionValidateOrder($params)
    {

        $order = $params['order'];
        $customer = $params['customer'];
        $cart = $params['cart'];
        
        // Naliczanie prowizji tylko od klientow szkoleniowca
        if ($customer->id_default_group == Configuration::get($this->prefix . 'klient_group')) {
            $prowizja = 0;
            $saldo = 0;
//       print_r($cart->getProducts(true));
//       exit();
            
            // Z kazdego produkta z koszyka naliczaj prowizje
            foreach ($cart->getProducts(true) as $cproduct) {
                // Cena dla szkoleniowca - cena dla klienta szkoleniowca 
                
                $prowizja = ((SpecificPrice::getSpecificPrice($cproduct['id_product'], $this->context->shop->id, $this->context->currency->id, $this->context->country->id, Configuration::get('mjtanexpert_klient_group'), 1, null, 0, 0, 0)['reduction']) == null) ? 0 : $cproduct['quantity'] * ((SpecificPrice::getSpecificPrice($cproduct['id_product'], $this->context->shop->id, $this->context->currency->id, $this->context->country->id, Configuration::get('mjtanexpert_expert_group'), 1, null, 0, 0, 0)['reduction'])-(SpecificPrice::getSpecificPrice($cproduct['id_product'], $this->context->shop->id, $this->context->currency->id, $this->context->country->id, Configuration::get('mjtanexpert_klient_group'), 1, null, 0, 0, 0)['reduction']));
                
                //$cproduct['quantity']*($cproduct['price_without_reduction']-$cproduct['price_with_reduction']);
                $saldo += $prowizja;
            }
            $query = 'INSERT INTO ' . _DB_PREFIX_ . 'tanexpert_saldo(id_order,id_customer_klient,saldo) VALUES("' . $order->id . '","' . $customer->id . '","' . $saldo . '")';
            DB::getInstance()->Execute($query, 1, 0);
            Logger::AddLog("Dodano prowizje do zamowienia " . $saldo);
            // p1 200 - 180 = 20
            // p2 100 - 150 = 50
            //= ''; // normalprice_cart - totalprice_discount;
            //echo "Klient group".Configuration::get('klient_group');
        }
//     print_r($cart->getProducts(true));
//     exit();
    }

    public function hookDisplayCustomerAccount()
    {
        return $this->fetch('module:' . $this->name . '/views/templates/front/hook/display_customer_account.tpl');
    }

    public function prepareNewTab($params)
    {
        $szkoleniowcy = array();
        $getSzkoleniowcy = TanexpertCustomer::getCustomersFromExpert($params['id_customer']);
        if (count($getSzkoleniowcy) > 0) {
            foreach ($getSzkoleniowcy as $key => $szkoleniowiec) {
                $szkoleniowcy[$key] = (new Customer($szkoleniowiec['id_customer_klient']));
            }
        }
        $this->context->smarty->assign(array(
            'expert_customers' => $szkoleniowcy,
            'languages' => $this->context->controller->_languages,
            'default_language' => (int) Configuration::get('PS_LANG_DEFAULT')
        ));
    }

    public function hookDisplayAdminCustomers($params)
    {
        if (Validate::isLoadedObject($customer = new Customer($params['id_customer']))) {
            $this->prepareNewTab($params);
//            return $this->display(__FILE__, 'views/templates/hook/video_url.tpl');
            return $this->fetch('module:' . $this->name . '/views/templates/admin/hook/display_admin_customer.tpl');
        }
    }

    // Budowanie formularza
    public function renderForm()
    {
        $orderStates = (new OrderState())->getOrderStates($this->context->language->id);
        $fields_form = array();
        $customers = (new Customer())->getCustomers(true);

        $filtered_customers = array();
        foreach ($customers as $key => $c) {
            $checkCustomer = (new Customer($c['id_customer']));
            
            if($checkCustomer->id_default_group != Configuration::get($this->prefix . 'expert_group')){
            $filtered_customers[$key]['id_customer'] = $c['id_customer'];
            $filtered_customers[$key]['email'] = $c['email'];
            }
        }
//        print_r($customers);
//        echo "<br/>******<br/>";
//        print_r($filtered_customers);
//        exit();
        
        $customer_group = (new Group())->getGroups($this->context->language->id, false);
        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Settings'),
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'label' => $this->l('Grupa klientów (Expert)'),
                    'name' => $this->prefix . 'expert_group',
                    'disabled' => false,
                    'required' => true,
                    'options' => array(
                        'query' => $customer_group,
                        'id' => 'id_group',
                        'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Grupa klientów (Klient)'),
                    'name' => $this->prefix . 'klient_group',
                    'disabled' => false,
                    'required' => true,
                    'options' => array(
                        'query' => $customer_group,
                        'id' => 'id_group',
                        'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Specjalny szkoleniowiec (Szkoleniowiec TT)'),
                    'name' => $this->prefix . 'szkoleniowiec_tt',
                    'disabled' => false,
                    'required' => true,
                    'options' => array(
                        'query' => $customer_group,
                        'id' => 'id_group',
                        'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Status dla anulowanego zamówienia'),
                    'name' => $this->prefix . 'order_canceled',
                    'disabled' => false,
                    'required' => true,
                    'options' => array(
                        'query' => $orderStates,
                        'id' => 'id_order_state',
                        'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Informacje dla Szkoleniowców'),
                    'name' => $this->prefix . 'info_szkoleniowiec',
                    'disabled' => false,
                    'required' => false,
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'name' => 'save_szkoleniowiec',
                'class' => 'btn btn-default pull-right',
            ),
        );

        $fields_form[1]['form'] = array(
            'legend' => array(
                'title' => $this->l('Dodaj klienta do experta'),
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'label' => $this->l('Expert'),
                    'name' => $this->prefix . 'expert_szkoleniowiec',
                    'disabled' => false,
                    'required' => false,
                    'options' => array(
                        'query' => $filtered_customers,
                        'id' => 'id_customer',
                        'name' => 'email',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Klient'),
                    'name' => $this->prefix . 'expert_klient',
                    'disabled' => false,
                    'required' => false,
                    'options' => array(
                        'query' => $filtered_customers,
                        'id' => 'id_customer',
                        'name' => 'email',
                    ),
                ),
            ),
            'submit' => array(
                'title' => $this->l('Add'),
                'icon' => 'process-icon-plus',
                'name' => 'dodaj_klienta',
                'class' => 'btn btn-default pull-right',
            ),
        );


        $form = new HelperForm();
//
        $form->token = Tools::getAdminTokenLite('AdminModules');

        $form->tpl_vars['fields_value'][$this->prefix . 'expert_group'] = Tools::getValue($this->prefix . 'expert_group', Configuration::get($this->prefix . 'expert_group'));
        $form->tpl_vars['fields_value'][$this->prefix . 'klient_group'] = Tools::getValue($this->prefix . 'klient_group', Configuration::get($this->prefix . 'klient_group'));

        $form->tpl_vars['fields_value'][$this->prefix . 'info_szkoleniowiec'] = Tools::getValue($this->prefix . 'info_szkoleniowiec', Configuration::get($this->prefix . 'info_szkoleniowiec'));


        $form->tpl_vars['fields_value'][$this->prefix . 'expert_szkoleniowiec'] = Tools::getValue($this->prefix . 'expert_szkoleniowiec', Configuration::get($this->prefix . 'expert_szkoleniowiec'));
        $form->tpl_vars['fields_value'][$this->prefix . 'expert_klient'] = Tools::getValue($this->prefix . 'expert_klient', Configuration::get($this->prefix . 'expert_klient'));
        $form->tpl_vars['fields_value'][$this->prefix . 'szkoleniowiec_tt'] = Tools::getValue($this->prefix . 'szkoleniowiec_tt', Configuration::get($this->prefix . 'szkoleniowiec_tt'));

        $form->tpl_vars['fields_value'][$this->prefix . 'order_canceled'] = Tools::getValue($this->prefix . 'order_canceled', Configuration::get($this->prefix . 'order_canceled'));


        
        return $form->generateForm($fields_form);
    }

    public function getContent()
    {
        return $this->postProcess() . $this->renderForm();
    }

    public function postProcess()
    {
        if (Tools::isSubmit('save_szkoleniowiec')) {
            Configuration::updateValue($this->prefix . 'expert_group', Tools::getValue($this->prefix . 'expert_group'));
            Configuration::updateValue($this->prefix . 'klient_group', Tools::getValue($this->prefix . 'klient_group'));
            Configuration::updateValue($this->prefix . 'expert_klient', Tools::getValue($this->prefix . 'expert_klient'));
            Configuration::updateValue($this->prefix . 'info_szkoleniowiec', Tools::getValue($this->prefix . 'info_szkoleniowiec'));
            Configuration::updateValue($this->prefix . 'order_canceled', Tools::getValue($this->prefix . 'order_canceled'));



            Configuration::updateValue($this->prefix . 'szkoleniowiec_tt', Tools::getValue($this->prefix . 'szkoleniowiec_tt'));

            return $this->displayConfirmation($this->l('Zapisano konfiguracje!'));
        }


        if (Tools::isSubmit('dodaj_klienta')) {
            $query = 'INSERT INTO ' . _DB_PREFIX_ . 'tanexpert_szkoleniowcy(id_customer_expert, id_customer_klient) VALUES(' . Tools::getValue($this->prefix . "expert_szkoleniowiec") . ',' . Tools::getValue($this->prefix . "expert_klient") . ')';

            $Klient = new Customer(Tools::getValue($this->prefix . "expert_klient"));
            $Klient->id_default_group = Configuration::get($this->prefix . 'klient_group');
            $Klient->save();
            $Klient->updateGroup([]);
            DB::getInstance()->Execute($query);

            return $this->displayConfirmation($this->l('Dodano klienta do szkoleniowca!'));
        }
    }

    public function hookActionOrderStatusPostUpdate($params)
    {
//        print_r($params);
//        
//        echo "<h1>".($params['id_order'])."</h1>";
//            exit();
        if ($params['newOrderStatus']->id == Configuration::get($this->prefix.'order_canceled')) {
            $query = 'DELETE FROM ' . _DB_PREFIX_ . 'tanexpert_saldo WHERE id_order = "'.$params['id_order'].'"';
                     DB::getInstance()->Execute($query, 1, 0);
            
        }
    }
    public function hookActionObjectUpdateAfter($params)
    {
        
        if(Tools::getValue('controller') == 'AdminOrders') {
            //echo "ORDER".Tools::getValue('id_order');
            $id_order = Tools::getValue('id_order');

            $biezace_saldo = TanexpertCustomer::getProwizjaFromZamowienie($id_order);
            $order = new Order($id_order);
            $obnizka = $order->total_discounts;
            // updating 
            $zmniejsz_prowizje = $biezace_saldo - $obnizka;
            $query = "UPDATE "._DB_PREFIX_."tanexpert_saldo SET saldo = '".$zmniejsz_prowizje."' WHERE id_order = '".$id_order."'";
            DB::getInstance()->Execute($query, 1, 0);
            //$query= "SELECT (ts.saldo) AS prowizja FROM "._DB_PREFIX_."tanexpert_saldo ts WHERE ts.id_order = '".pSQL($id_order)."'";
 
//        print_r($params);
//        exit();
        }
    }
}
