<?php
/**
 * Getting Tanexpert customers
 *
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */
 
class TanexpertCustomer extends Module
{
    public function getCustomer($id)
    {
        $query = "SELECT * FROM "._DB_PREFIX_."customer WHERE id_customer = '".pSQL($id)."'";
        return DB::getInstance()->ExecuteS($query, 1, 0);
    }
    public static function getCustomerById($id) { 
        $query = "SELECT * FROM "._DB_PREFIX_."customer WHERE id_customer = '".pSQL($id)."'";
        return DB::getInstance()->ExecuteS($query, 1, 0);
    }
    public function getCustomersFromExpert($id) {
        ////SELECT * FROM pstan_customer c JOIN pstan_tanexpert_szkoleniowcy ts ON c.id_customer = ts.id_customer_klient WHERE ts.id_customer_expert = '3' AND c.id_lang = '1'
        $query= "SELECT * FROM "._DB_PREFIX_."customer c JOIN "._DB_PREFIX_."tanexpert_szkoleniowcy ts ON c.id_customer = ts.id_customer_klient WHERE ts.id_customer_expert = '".pSQL($id)."' AND c.id_lang = '".Context::getContext()->language->id."'";
        return DB::getInstance()->ExecuteS($query, 1, 0);
    }
    public function getSzkoleniowcy($id_default_group)
    {
        $query = "SELECT * FROM "._DB_PREFIX_."customer WHERE id_default_group = '".pSQL($id_default_group)."' AND id_lang = '".Context::getContext()->language->id."'";
        return DB::getInstance()->ExecuteS($query, 1, 0);
    }
    public static function getQtyCustomersExpert($id) {
        $query= "SELECT * FROM "._DB_PREFIX_."customer c JOIN "._DB_PREFIX_."tanexpert_szkoleniowcy ts ON c.id_customer = ts.id_customer_expert WHERE id_customer_expert = '".pSQL($id)."'";
        return count(DB::getInstance()->ExecuteS($query, 1, 0));
    }
    public static  function getProwizjeFromExpert($id) {
        $query= "SELECT SUM(ts.saldo) AS prowizja FROM "._DB_PREFIX_."tanexpert_saldo ts LEFT JOIN "._DB_PREFIX_."tanexpert_szkoleniowcy tsz ON tsz.id_customer_klient = ts.id_customer_klient WHERE tsz.id_customer_expert = '".pSQL($id)."'";
        return DB::getInstance()->ExecuteS($query, 1, 0)[0]['prowizja'];
    }
    public static function getProwizjaFromZamowienie($id_order) {
        //FROM pstan_tanexpert_saldo
        $query= "SELECT (ts.saldo) AS prowizja FROM "._DB_PREFIX_."tanexpert_saldo ts WHERE ts.id_order = '".pSQL($id_order)."'";
        return DB::getInstance()->ExecuteS($query, 1, 0)[0]['prowizja'];
    }
    public static function getProwizjeFromKlient($id) { 
        $query= "SELECT SUM(ts.saldo) AS prowizja FROM "._DB_PREFIX_."tanexpert_saldo ts LEFT JOIN "._DB_PREFIX_."tanexpert_szkoleniowcy tsz ON tsz.id_customer_klient = ts.id_customer_klient WHERE tsz.id_customer_klient = '".pSQL($id)."'";
        return DB::getInstance()->ExecuteS($query, 1, 0)[0]['prowizja'];
    }
    public static function getAktualneSaldoFromExpert($id) {
        $query= "SELECT SUM(ts.saldo) AS prowizja FROM "._DB_PREFIX_."tanexpert_saldo ts LEFT JOIN "._DB_PREFIX_."tanexpert_szkoleniowcy tsz ON tsz.id_customer_klient = ts.id_customer_klient WHERE tsz.id_customer_expert = '".pSQL($id)."'";
        $sumaProwizji = DB::getInstance()->ExecuteS($query, 1, 0)[0]['prowizja'];
            if($sumaProwizji == null) {
                $sumaProwizji = 0;
            }
            
        $query2= "SELECT SUM(tf.prowizja) AS prowizjafv FROM "._DB_PREFIX_."tanexpert_faktury tf WHERE tf.id_customer_expert = '".pSQL($id)."'";
        $sumaFaktur = DB::getInstance()->ExecuteS($query2, 1, 0)[0]['prowizjafv'];
//        
         if($sumaFaktur == null) {
                $sumaFaktur = 0;
            }
            
            
        $biezaceSaldo = $sumaProwizji - $sumaFaktur;
        
        return $biezaceSaldo;
    }
    public static function getOrderDate($id) {
        $order = new Order($id);
        return $order->date_add;
    }
    public function addInvoice($id_customer_expert, $invoice, $commision)  {
        $query = 'INSERT INTO '._DB_PREFIX_.'tanexpert_faktury(prowizja, id_customer_expert, faktura, data_dodania) VALUES("'.pSQL($commision).'","'.pSQL($id_customer_expert).'","'.pSQL($invoice).'", "'.date("Y-m-d").'")';
        return DB::getInstance()->Execute($query, 1, 0);
    }
    public function getInvoices($id_customer_expert) {
        
//        echo $id_customer_expert;
//        exit();
             $query = 'SELECT * FROM '._DB_PREFIX_.'tanexpert_faktury WHERE id_customer_expert = "'.pSQL($id_customer_expert).'"';
        return DB::getInstance()->ExecuteS($query, 1, 0);
    }
}
