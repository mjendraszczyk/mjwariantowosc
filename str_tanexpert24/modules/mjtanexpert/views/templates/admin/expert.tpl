<div class="panel">
<table class="table">
<tr style='height: 50px;font-weight: bold;'>
<td>
ID
</td>
<td>
Imię i nazwisko
</td>
<td>
E-mail
</td>
<td>
Łączna wartość prowizji
</td>
<td>
Opcje
</td>
</tr>
{*{$szkoleniowcy|print_r}*}
{foreach from=$szkoleniowcy item=szkoleniowiec}
    <tr>
        <td>
            {$szkoleniowiec['id_customer']}
        </td>
        <td>
            {$szkoleniowiec['firstname']} 
            {$szkoleniowiec['lastname']}
        </td>
        <td>
            {$szkoleniowiec['email']}
        </td>
        <td>
            {Tools::displayPrice(TanexpertCustomer::getProwizjeFromExpert($szkoleniowiec['id_customer']))}
        </td>
        <td>
            <a href='{$link->getAdminLink("AdminTanexpertcustomer",true,[],["show"=>"customers","id_customer"=>$szkoleniowiec['id_customer']])}' class='btn btn-default'>
                <i class='icon-users'></i>
                {l s='View'} ({TanexpertCustomer::getQtyCustomersExpert($szkoleniowiec['id_customer'])}) klientów</a>
                <a href='{$link->getAdminLink("AdminTanexpertcustomer",true,[],["show"=>"orders","id_customer"=>$szkoleniowiec['id_customer']])}' class='btn btn-default'>
                <i class='icon-shopping-cart'></i>
                {l s='View'} {l s='Orders'}</a>
                
                <a href="{$link->getAdminLink("AdminTanexpertcustomer",true,[],["show"=>"invoices","id_customer"=>$szkoleniowiec['id_customer']])}" class="btn btn-info">
                    <i class="icon icon-list"></i>
                    {l s='Lista faktur' mod='mjtanexpert'}
                </a>
                {*{$link->getAdminLink("AdminCustomers",true,['/3/edit'],[])}*}
                
            <a href='{$link->getLegacyAdminLink("AdminCustomers",false,['id_customer'=>$szkoleniowiec['id_customer']])}&updatecustomer&token={Tools::getAdminTokenLite('AdminCustomers')}' class='btn btn-default'>
                <i class='icon-edit'></i>
                {l s='Edit'}</a>
        </td>
    </tr>
    {/foreach}
</table>
</div>