<div class="panel">
<div class="col-md-8">
<h3>Zamówienia szkoleniowca: 
{foreach from=TanexpertCustomer::getCustomerById(Tools::getValue('id_customer')) item=expert}
    {$expert['email']}
    {/foreach}
    </h3>
</div>
    <div class="col-md-4">
    <a href="{$link->getAdminLink("AdminTanexpertcustomer",true)}" class="btn btn-default pull-right">
        <i class="icon icon-chevron-left"></i> Powrót</a>
    </div>
<table class="table">
<tr style='height: 50px;font-weight: bold;'>
<td>
ID zamówienia
</td>
<td>
Imię i nazwisko
</td>
<td>
E-mail
</td>
<td>
Prowizja
</td>
<td>
Data zamówienia
</td>
<td>
    Opcje
</td>
</tr>
{foreach from=$zamowienia item=zamowienie}
    <tr>
        <td>
            {$zamowienie['id_order']}
        </td>
        <td>
            {$zamowienie['firstname']} 
            {$zamowienie['lastname']}
        </td>
        <td>
            {$zamowienie['email']}
        </td>
        <td>
            {Tools::displayPrice($zamowienie['saldo'])}
        </td>
        <td>
           {TanexpertCustomer::getOrderDate($zamowienie['id_order'])}
        </td>
        <td>
            <a class="btn btn-default" href="{$link->getLegacyAdminLink("AdminOrders",true,['vieworder'=>'','id_order'=>$zamowienie['id_order']])}">{l s='View'}</a>
        </td>
    </tr>
    {/foreach}
</table>
</div>