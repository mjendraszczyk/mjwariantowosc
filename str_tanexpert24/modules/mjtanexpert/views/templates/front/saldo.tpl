{extends file='customer/page.tpl'}

{block name='page_title'}
  {l s='Program afiliacyjny' d='Shop.Theme.Customeraccount'}
{/block}


{block name='page_content'}
    <h6>Informacje o programie afiliacyjnym</h6>
    <div class="row card" style='padding:25px;margin:20px 0;'>
    <div class="panel">
    <div class="panel-body">
    <div class="col-md-8">
{Configuration::get('mjtanexpert_info_szkoleniowiec')}
    </div>
    <div class='col-md-4 green-text'>
        <div class="inline middle">
        <i class='material-icons'>attach_money</i>
        </div>
        <div class="inline-block middle">
        <h6 class='inline  margin0'>Dostępne saldo</h6>
        <h3>{Tools::displayPrice(TanexpertCustomer::getAktualneSaldoFromExpert(Context::getContext()->customer->id))}</h3>
        </div>
    </div>
</div></div></div>

<table class="table">
<tr style='height: 90px;font-weight: bold;background: #4faaa4;
    color: #fff;'>
    <td style="vertical-align: middle;">
Referencja zamówienia
</td>
<td style="vertical-align: middle;">
ID zamówienia
</td>
<td style="vertical-align: middle;">
E-mail
</td>
<td style="vertical-align: middle;">
Prowizja
</td>
<td style="vertical-align: middle;">
Wartość zamówienia
</td>
<td style="vertical-align: middle;">
Data zamówienia
</td>
</tr>

{foreach from=$orders_detail key=x item=saldo}
<tr>
    <td>
        {$saldo['reference']}
    </td>
    <td>
        {$saldo['id_order']}
    </td>
    <td>
        {$saldo['email']}
    </td>
    <td>
        {Tools::displayPrice($saldo['saldo'])}
    </td>
    <td>
        {Tools::displayPrice($saldo['value'])}
    </td>
    <td>
       {TanexpertCustomer::getOrderDate($saldo['id_order'])}
    </td>
</tr>
<tr style='background: #eee;'>
    <td colspan="6">
<strong>Lista produktów</strong>
<ul>
        {*{$saldo['products']|print_r}*}
        {foreach from=$saldo['products'] item=produkt}
            {if $produkt['id_order'] == $saldo['id_order']}
        <li>{$produkt['product_name']} x {$produkt['product_quantity']} szt</li>
        {/if}
        
        {/foreach}
    
</ul>
        </td>
</tr>
{/foreach}
</table>
{/block}