{extends file='customer/page.tpl'}

{block name='page_title'}
  {l s='Wypłać saldo' d='Shop.Theme.Customeraccount'}
{/block}


{block name='page_content'}
    <h6>Informacje o programie afiliacyjnym</h6>
    <div class="row card" style='padding:25px;'>
    <div class="panel">
    <div class="panel-body">
    <div class="col-md-8">
{Configuration::get('mjtanexpert_info_szkoleniowiec')}
    </div>
    <div class='col-md-4 green-text'>
        <div class="inline middle">
        <i class='material-icons'>attach_money</i>
        </div>
        <div class="inline-block middle">
        <h6 class='inline  margin0'>Dostępne saldo</h6>
        <h3>{Tools::displayPrice(TanexpertCustomer::getAktualneSaldoFromExpert(Context::getContext()->customer->id))}</h3>
        </div>
    </div>
</div></div></div>

    {if isset($success)}
        <div class="alert alert-success">
        {$success}
        </div>
        {/if}

        {if isset($error)}
            <div class="alert alert-danger">
                {$error}
            </div>
        {/if}
     <form method="post"  enctype="multipart/form-data">
        <label>Podaj wartość</label>
        <input type="number" name="prowizja" min="0" max="{TanexpertCustomer::getAktualneSaldoFromExpert(Context::getContext()->customer->id)}" placeholder="Podaj wartość prowizji (max {Tools::displayPrice(TanexpertCustomer::getAktualneSaldoFromExpert(Context::getContext()->customer->id))})" class="form-control"/>
        <label>Wgraj fakturę</label>
        <input type="file" name="invoice" class="form-control">
        <button type="submit" name="sendInvoice" class="btn btn-primary" style="margin:20px auto;display:block;padding:25px 50px;">Wypłać środki</button>
    </form>
        
        <h1>
            {l s='Lista faktur' d='Shop.Theme.Customeraccount'}
        </h1>
            <table class="table">
<tr style='height: 50px;font-weight: bold;'>
<td>
ID
</td>
<td>
Wartość
</td>
<td>
    Data wystawienia
</td>
<td>
Pobierz fakturę
</td>
 
</tr>
{foreach from=$invoices item=invoice}
<tr>
    <td>
        {$invoice['id_tanexpert_faktury']}
    </td>
    <td>
        {Tools::displayPrice($invoice['prowizja'])}
    </td>
    <td>
        
    </td>
    <td>
    <a href="http://{{Tools::getShopDomain(false, false)}}/modules/mjtanexpert/controllers/front/uploads/invoices/{$invoice['faktura']}" class="btn btn-secondary" target="_blank">Pobierz fakturę</a>    
    </td>
</tr>
    {/foreach}
            </table>
{/block}