<?php

if (!defined('_PS_VERSION_'))
	exit;

require_once dirname(__FILE__) . '/classes/MjWariant.php';

class Mjwariantowosc extends Module
{

	private $_html = '';

	function __construct()
	{
		$this->name = 'mjwariantowosc';
		$this->tab = 'front_office_features';
		$this->version = '1.0';
		$this->author = 'MAGES';
		$this->need_instance = 0;
		$this->table_name = 'product_wariantowosc';

		$this->bootstrap = true;

	 	parent::__construct();

		$this->displayName = $this->l('Wielowariantowe zakupy produktów');
		$this->description = $this->l('Kup w jednym produkcie zestaw wielu produktów');

	}

	public function install()
	{
		if (!parent::install() OR
			!$this->_installTable() OR
                        !$this->registerHook('displayProductWariants') OR
                        !$this->registerHook('displayAdminOrderContentOrder') OR
                        !$this->registerHook('displayAdminOrder') OR
                        !$this->registerHook('displayOrderDetail') OR
                        !$this->registerHook('displayProductActions') OR
                        !$this->registerHook('displayShoppingCart') OR
                        !$this->registerHook('displayCartExtraProductActions') OR
                        !$this->registerHook('displayProductPriceBlock') OR
                        !$this->registerHook('displayUserSet') OR
                        !$this->registerHook('actionCartSave') OR
			!$this->registerHook('displayAdminProductsExtra') OR
                        !$this->registerHook('actionValidateOrder') OR
			!$this->registerHook('actionProductUpdate'))
			return false;
		return true;
	}

	public function uninstall()
	{
            //OR !$this->_eraseTable())
		if (!parent::uninstall())
			return false;
		return true;
	}

	private function _installTable()
	{
            // ile wariantÃ³w | id_produktu | stan | id_produktow
		$sql = '
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.$this->table_name .'` (
                                `id_wariant` INT(12) NOT NULL AUTO_INCREMENT,
				`id_product` INT(12) NOT NULL,
				`liczba_wariantow` INT(12) NOT NULL,
                                `stan` INT(12) NOT NULL,
                                `ids_produktow` TEXT NULL, PRIMARY KEY ( `id_wariant` )
				) ENGINE = ' ._MYSQL_ENGINE_;
                
                $sql2 = '
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'product_wariantowosc_orders`(
                                `id_wariant_order` INT(12) NOT NULL AUTO_INCREMENT,
                                `id_order` INT(12) NOT NULL,
				`lista` TEXT NULL,
				 PRIMARY KEY ( `id_wariant_order` )
				) ENGINE = ' ._MYSQL_ENGINE_;
                
                $sql3 = '
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'product_wariantowosc_kategorie`(
                                `id_wariant_kategorie` INT(12) NOT NULL AUTO_INCREMENT,
                                `id_product` INT(12) NOT NULL,
                                `id_wariant` INT(12) NOT NULL,
                                `id_category` INT(12) NOT NULL,
				 PRIMARY KEY ( `id_wariant_kategorie` )
				) ENGINE = ' ._MYSQL_ENGINE_;
		if(!Db::getInstance()->Execute($sql) || (!Db::getInstance()->Execute($sql2)) || (!Db::getInstance()->Execute($sql3)))
			return false;
		return true;
	}

	private function _eraseTable()
	{
		if(!Db::getInstance()->Execute('DROP TABLE `'._DB_PREFIX_.$this->table_name.'`'))
			return false;
		return true;
	}

	public function hookActionProductUpdate($params)
	{
 
            $id_product = $params['id_product'];
            
            $accesories = Product::getAccessoriesLight($this->context->language->id, $id_product);
            $ids_produktow =  array();
            foreach ($accesories as $key => $accesory) {
                $ids_produktow[$key] = $accesory['id_product'];
            }
            $ids_produktow_all = implode(",",$ids_produktow);
                        
            if(MjWariant::czyIstnieje($id_product) == 0) {
                MjWariant::dodajWarianty($params['id_product'], Tools::getValue('liczba_wariantow'), Tools::getValue('stan'), $ids_produktow_all);
                
                for($i=1;$i<=Tools::getValue('liczba_wariantow');$i++) {
                    if(MjWariant::sprawdzKategorieWariant($params['id_product'], $i) > 0) {
                       MjWariant::aktualizujKategorieWariant($params['id_product'], $i, Tools::getValue('kategoria_wariant_'.$i));
                   } else{
                       MjWariant::dodajKategorieWariant($params['id_product'], $i, Tools::getValue('kategoria_wariant_'.$i));
                   }
                }
                return  true;
            } else {
               MjWariant::aktualizujWarianty($params['id_product'], Tools::getValue('liczba_wariantow'), Tools::getValue('stan'), $ids_produktow_all);
               for($i=1;$i<=Tools::getValue('liczba_wariantow');$i++) {
                   if(MjWariant::sprawdzKategorieWariant($params['id_product'], $i) > 0) {
                       MjWariant::aktualizujKategorieWariant($params['id_product'], $i, Tools::getValue('kategoria_wariant_'.$i));
                   } else{
                       MjWariant::dodajKategorieWariant($params['id_product'], $i, Tools::getValue('kategoria_wariant_'.$i));
                   }
                }
               return  true;
            }
	}

        public function hookDisplayProductWariants() {
            $id_product = Tools::getValue('id_product');
            if(MjWariant::czyIstnieje($id_product)) {
            
            $pobierzWarianty = MjWariant::pobierzWarianty($id_product);
//            print_r($pobierzWarianty );
//            exit();
            $tablica_produktow = array();
            foreach($pobierzWarianty as $wariant) {
                $this->context->smarty->assign('liczba_wariantow', $wariant['liczba_wariantow']);
                $this->context->smarty->assign('stan', $wariant['stan']);
                
                $ids_produktow = explode(",",$wariant['ids_produktow']);
                
                foreach($ids_produktow as $key => $ids) {
                    $tablica_produktow[$key]['id_product'] = $ids;
                    $tablica_produktow[$key]['name'] = Product::getProductName($ids);
                    $tablica_produktow[$key]['id_category'] = (new Product($ids))->id_category_default;
                }
            }
            
            
            $this->context->smarty->assign('tablica_produktow', $tablica_produktow);
            $this->context->smarty->assign('id_product', $id_product);
            //echo "TESTTESTTEST:".$id_product;
            return $this->fetch('module:' . $this->name . '/views/templates/hook/displayProductWariants.tpl');
            
            } else {
                return false;
            }
        }

        public function hookDisplayAdminProductsExtra($params)
	{	
		$groups = Group::getGroups($this->context->language->id);
                
                $accesories = Product::getAccessoriesLight($this->context->language->id, $params['id_product']);
		
		$this->context->smarty->assign('warianty', (new MjWariant())->pobierzWarianty($params['id_product']));
                $this->context->smarty->assign('accessories', $accesories);
                $this->context->smarty->assign('id_product', $params['id_product']);
                
		return $this->display(__FILE__, 'adminProductsExtra.tpl');
	}
        public function hookActionCartSave() {
            
            if(MjWariant::czyIstnieje(Tools::getValue('id_product_wariant')) != 0) {
                for($i=1;$i<=Tools::getValue('liczba_wariantow');$i++) {
                            $nazwa_ciastka = "product_".Tools::getValue('id_product_wariant')."_wariant_".$i;
                            $this->context->cookie->$nazwa_ciastka = Tools::getValue("product_".Tools::getValue('id_product_wariant').'_wariant_'.$i);
                }
            }
        }

	public function enable($force_all = false)
	{
		Tools::clearCache();
		parent::enable();
	}

	public function disable($force_all = false)
	{
		Tools::clearCache();
		parent::disable();
	}
          
    public function hookDisplayAdminOrderContentOrder()
    {
        //echo "hookDisplayAdminOrderContentOrder";
    }

    public function hookDisplayAdminOrder()
    {
        $message = (new MjWariant())->getWariantMessage(Tools::getValue('id_order'));
        
        $wiadomosc = '';
        foreach($message as $m) {
            $wiadomosc .= "<br/><br/>";
            $wiadomosc .= $m['lista'];
        }
                        $this->context->smarty->assign('message', $wiadomosc);

        return $this->fetch('module:' . $this->name . '/views/templates/hook/displayAdminOrder.tpl');
    }

    public function hookDisplayOrderDetail()
    {
        //echo "hookDisplayOrderDetail";
        
    }

    public function hookDisplayProductActions($params)
    {
        
             //   if(MjWariant::czyIstnieje($params['id_product']) != 0) {
                    
       // echo "hookDisplayProductActions:".$this->context->cookie->wariant_1.", ".$this->context->cookie->wariant_2.", ".$this->context->cookie->wariant_3;
             //   } else {
               //     return false;
              //  }
    }

    public function hookDisplayShoppingCart()
    {
//        if(MjWariant::czyIstnieje($params['id_product']) != 0) {
//                    
//        echo "displayShoppingCart".Context::getContext()->cookie->wariant_0;
//        }
    }
        public function hookDisplayCartExtraProductActions($params) {
            //if(MjWariant::czyIstnieje($params['id_product']) != 0) {
            //echo "hookDisplayCartExtraProductActions:".Product::getProductName($this->context->cookie->wariant_1).", ".Product::getProductName($this->context->cookie->wariant_2).", ".Product::getProductName($this->context->cookie->wariant_3);
            //} else {
             //   return false;
            //}
        }
        public function hookDisplayUserSet($params) {
            //if(MjWariant::czyIstnieje($params['id_product']) != 0) {
            if($params) {
                $id = $params['id_product'];
                
            } else {
                $id=Tools::getValue('id_product');
//                print_r($params);
//                exit();
            }
            $pobierzWarianty = (new MjWariant())->pobierzWarianty($id);
            //$content = "<ul class='mjwariantowosc_user_set'>";
            
            $ids_produktow = array();
            foreach($pobierzWarianty as $key => $wariant) {
                $ids_produktow = explode(",",$wariant['ids_produktow']);
            }
            $zestaw = array();
            foreach($ids_produktow as $key => $ids) {
                $nazwa_wariantu = "product_".$id."_wariant_".($key+1);
                 if($this->context->cookie->$nazwa_wariantu != '') {
                $zestaw[$key] = Product::getProductName($this->context->cookie->$nazwa_wariantu);
                 }
                 
                
            }
            //$content .= "</ul>";
            
            $this->context->smarty->assign('zestaw', $zestaw);
//            $this->context->smarty->assign('id_product', $id_product);
            //echo "TESTTESTTEST:".$id_product;
            return $this->fetch('module:' . $this->name . '/views/templates/hook/displayUserSet.tpl');
            //echo "hookDisplayProductPriceBlock:".Product::getProductName($this->context->cookie->wariant_1).", ".Product::getProductName($this->context->cookie->wariant_2).", ".Product::getProductName($this->context->cookie->wariant_3);
           // }
//            else{ 
//                return false;
//            }
        }
    public function hookActionValidateOrder($params)
    {
        $order = $params['order'];
        $cart = $params['cart'];
        $customer = $params['customer'];
        
        $wiadomosc = '';
        $zamowienie = (new Order($order->id))->getProducts();
//        print_r($zamowienie);
//        exit();
        foreach ($zamowienie as $product) {
          //  echo $product['product_id'];
           //
           //exit();
            if(MjWariant::czyIstnieje($product['product_id']) > 0) { 
                $message = new Message();
                $message->id_order = $order->id;
                $message->id_customer = $customer->id;
                $message->id_cart = $cart->id;
                
                $wiadomosc = "\n";
                $wiadomosc .= "Wybrany zestaw:\n";
                
                $pobierzWarianty = (new MjWariant())->pobierzWarianty($product['product_id']);
            
                $ids_produktow = array();
                foreach($pobierzWarianty as $key => $wariant) {
                    $ids_produktow = explode(",",$wariant['ids_produktow']);
                }
                $zestaw = array();
                foreach($ids_produktow as $key => $ids) {
                    $nazwa_wariantu = "product_".$product['product_id']."_wariant_".($key+1);
                    if($this->context->cookie->$nazwa_wariantu != '') {
                        $zestaw[$key] = Product::getProductName($this->context->cookie->$nazwa_wariantu);
                 }
            }
            foreach($zestaw as $z) {
                $wiadomosc .= "<br/>";
                $wiadomosc .= $z."\n";
                //$wiadomosc .= "</li>";
            }
            $message->message = $wiadomosc;
            $message->private = false;
            $message->save();
            
            
            $customerThread = new CustomerThread();
            $customerThread->id_shop = $this->context->shop->id;
            $customerThread->id_lang = $this->context->language->id;
            $customerThread->id_contact = 1;
            $customerThread->id_customer = $this->context->customer->id;
            $customerThread->id_order = $order->id;
            $customerThread->token = Tools::getValue('token');
            $customerThread->save();
            

            $customerMessage = new CustomerMessage();
            $customerMessage->id_customer_thread = $customerThread->id;
            $customerMessage->id_employee = '0';
            $customerMessage->message = $wiadomosc;
            $customerMessage->private = '0';
            $customerMessage->date_add = date('Y-m-d H:i:s');
            $customerMessage->date_upd = date('Y-m-d H:i:s');
            $customerMessage->read = false;
            $customerMessage->save();
            
             $query = 'INSERT INTO ' . _DB_PREFIX_ . 'product_wariantowosc_orders(id_order,lista) VALUES("' . $order->id . '","'.$wiadomosc.'")';
            DB::getInstance()->Execute($query, 1, 0);
            
                foreach($ids_produktow as $key => $ids) {
                    $nazwa_wariantu = "product_".$product['product_id']."_wariant_".($key+1);
                    if($this->context->cookie->$nazwa_wariantu != '') {
                       $this->context->cookie->$nazwa_wariantu = null;
                 }
            }
            
            
            if (Validate::isEmail(Configuration::get('PS_SHOP_EMAIL'))) {
            Mail::Send(
                    Configuration::get('PS_LANG_DEFAULT'), // id lang
                    'contact', // template 
                    ' Zestawy dla zamÃ³wienia: '.$order->reference, // subject
                    array(
                '{email}' => Configuration::get('PS_SHOP_EMAIL'), // sender email address
                '{message}' => $wiadomosc, // template vars
                '{order_name}' => 'ZamÃ³wienie '.$order->reference,
                '{attached_file}' => ''
                    ), Configuration::get('PS_SHOP_EMAIL'), // to //
                    null, //$to_name = 
                    null, //$from = 
                    null, //$from_name = 
                    null, //$file_attachment = 
                    null, //$mode_smtp = 
                    _PS_MAIL_DIR_, //$template_path = 
                    false, //$die = 
                    null, //$id_shop
                    null, //$bcc
                    null // reply_to
            );
        }
        
            }
        }

    }

}
//Syntax highlighting powered by Luminous
