
<div id="product-mjwariantowosc" class="panel product-tab">

	<input type="hidden" name="submitted_tabs[]" value="mjwariantowosc">
	<h3>{l s='Wielowariantowość' mod='mjwariantowosc'}</h3>
	<div class="form-group">
            <label class="control-label col-lg-3">
			<span class="label-tooltip" data-toggle="tooltip" data-html="true" title="" data-original-title="{l s='Podaj liczbę kombinacji' mod='mjwariantowosc'}">
                           Ile ma być kombinacji produktów</span>
            </label>
            {if MjWariant::czyIstnieje($id_product) == 1}
                {foreach from=$warianty item=wariant}
                    <input type="number" value="{$wariant['liczba_wariantow']}" name="liczba_wariantow" min="0" class="form-control"/>
                    {/foreach}
                {else}
                    <input type="number" name="liczba_wariantow" min="0" class="form-control"/>
                {/if}
                
                
                {foreach from=$warianty item=wariant}
                    {if $wariant['liczba_wariantow'] != ''}
                        {for $kategoria=1 to $wariant['liczba_wariantow']}
                            <label>Kategoria dla wariantu {$kategoria}:</label>
                            <select name="kategoria_wariant_{$kategoria}" class='form-control'>
                                {foreach from=MjWariant::pobierzKategoriezZestawow($id_product) item=k}
                                <option value='{$k['id_category']}'>{$k['name']}</option>
                                {/foreach}
                            </select>
                                {/for}
                        {/if}
                    {/foreach}

            <label class="control-label col-lg-3">
			<span class="label-tooltip" data-toggle="tooltip" data-html="true" title="" data-original-title="{l s='Podaj czy włączyć kombinacje dla tego produktu' mod='mjwariantowosc'}">
                           Włączyć tryb kombinacji?</span>
            </label>
                           
                             {if MjWariant::czyIstnieje($id_product) == 1}
                {foreach from=$warianty item=wariant}
                    <select name="stan" class="form-control">
                        {if $wariant['stan'] == '1'}
                               <option value="1" selected="selected">Włączone</option>
                               <option value="0" >Wyłączone</option>
                               {else}
                                   <option value="1">Włączone</option>
                                   <option value="0" selected="selected">Wyłączone</option>
                               {/if}
                               
                           </select>
                    {/foreach}
                {else}
                     <select name="stan" class="form-control">
                               <option value="1">Włączone</option>
                               <option value="0">Wyłączone</option>
                           </select>
                {/if}

                          
                           
                        {*{$accessories|print_r}
                        {*test: {MjWariant::czyIstnieje('90')}
                        T: {$warianty|print_r}*}
		{*<label class="control-label col-lg-3">
			<span class="label-tooltip" data-toggle="tooltip" data-html="true" title="" data-original-title="{l s='Mark all of the customer groups which you would like to have access to this product.' mod='psproductaccess'}">Group access</span>
		</label>
		<div class="col-lg-9">
			<div class="row">
				<div class="col-lg-6">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th class="fixed-width-xs">
									<span class="title_box">
										<input type="checkbox" name="checkme" id="checkme" onclick="checkDelBoxes(this.form, 'acc_groupBox[]', this.checked)">
									</span>
								</th>
								<th class="fixed-width-xs"><span class="title_box">ID</span></th>
								<th>
									<span class="title_box">
										Group name
									</span>
								</th>
							</tr>
						</thead>
						<tbody>
							
							<input type="hidden" name="acc_groupBox[]" value="0"> 
                                                        
							{foreach from=$access_groups item=group}
								<tr>
									<td>
										<input type="checkbox" name="acc_groupBox[]" class="acc_groupBox" id="acc_groupBox_{$group.id_group}" value="{$group.id_group}" {if isset($group.access)} checked="checked"{/if}>
									</td>
									<td>{$group.id_group}</td>
									<td>
										<label for="acc_groupBox_{$group.id_group}">{$group['name']}</label>
									</td>
								</tr>
							{/foreach}
						</tbody>
					</table>
				</div>
			</div>
		</div>*}
							
	</div>

	{*<div class="panel-footer">
		<a href="{$link->getAdminLink('AdminProducts')}" class="btn btn-default"><i class="process-icon-cancel"></i> {l s='Cancel'}</a>
		<button type="submit" name="submitAddproduct" class="btn btn-default pull-right"><i class="process-icon-save"></i> {l s='Save'}</button>
		<button type="submit" name="submitAddproductAndStay" class="btn btn-default pull-right"><i class="process-icon-save"></i> {l s='Save and stay'}</button>
	</div>*}

</div>
