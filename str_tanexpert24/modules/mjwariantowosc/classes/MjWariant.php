<?php
/**
 * Mjwariantowosc class
 *
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

class MjWariant extends Module {


    public static function czyIstnieje($id_product) {
        $sql = 'SELECT * FROM '._DB_PREFIX_.'product_wariantowosc WHERE id_product = "'.pSQL($id_product).'"';
        
        $ilosc = count(DB::getInstance()->ExecuteS($sql, 1, 0));
        
        return $ilosc;
    }

    public static function dodajWarianty($id_product, $liczba_wariantow, $stan, $ids_produktow) {
        $sql = 'INSERT INTO '._DB_PREFIX_.'product_wariantowosc(id_product, liczba_wariantow, stan, ids_produktow) VALUES("'.pSQL($id_product).'","'.pSQL($liczba_wariantow).'","'.pSQL($stan).'","'.pSQL($ids_produktow).'")';
        
        return DB::getInstance()->Execute($sql, 1, 0);
    }
    public static function aktualizujWarianty($id_product, $liczba_wariantow, $stan, $ids_produktow) {
        $sql = 'UPDATE '._DB_PREFIX_.'product_wariantowosc SET id_product = "'.pSQL($id_product).'", liczba_wariantow = "'.pSQL($liczba_wariantow).'", stan = "'.pSQL($stan).'", ids_produktow = "'.pSQL($ids_produktow).'" WHERE id_product = "'.$id_product.'"';
        
        return DB::getInstance()->Execute($sql, 1, 0);
    }
    public function pobierzWarianty($id_product) {
        
        $sql = 'SELECT * FROM '._DB_PREFIX_.'product_wariantowosc WHERE id_product = "'.pSQL($id_product).'"';
        
        return DB::getInstance()->ExecuteS($sql, 1, 0);
    }
    public function getWariantMessage($id_order) {
        $sql = 'SELECT * FROM '._DB_PREFIX_.'product_wariantowosc_orders WHERE id_order = "'.pSQL($id_order).'"';
        
        return DB::getInstance()->ExecuteS($sql, 1, 0);
    }
    public static function pobierzKategoriezZestawow($id_product) {
        $accesories = Product::getAccessoriesLight(Context::getContext()->language->id, $id_product);
        
        $kategorie = array();
        $sprawdz_kategorie = array();
        foreach($accesories as $key => $a) { 
            $product = new Product($a['id_product']);
            if(!in_array($product->id_category_default, $sprawdz_kategorie)) {
                $sprawdz_kategorie[$key] = $product->id_category_default;
            $kategorie[$key]['id_category'] = $product->id_category_default;
            $kategorie[$key]['name'] = (new Category($product->id_category_default))->getName();
            }
        }
        return $kategorie;
    }
//    `id_wariant_kategoria` INT(12) NOT NULL AUTO_INCREMENT,
//                                `id_product` INT(12) NOT NULL,
//				`id_wariant` INT(12) NOT NULL,
//                                `id_category` INT(12) NOT NULL,
    public static function sprawdzKategorieWariant($id_product, $id_wariant) {
        $sql = 'SELECT * FROM '._DB_PREFIX_.'product_wariantowosc_kategorie WHERE id_product = "'.pSQL($id_product).'" AND id_wariant = "'.pSQL($id_wariant).'"';
        
        return count(DB::getInstance()->ExecuteS($sql, 1, 0));
    }
    public static function dodajKategorieWariant($id_product, $id_wariant, $id_category) {
        $sql = 'INSERT INTO '._DB_PREFIX_.'product_wariantowosc_kategorie(id_product, id_wariant, id_category) VALUES("'.pSQL($id_product).'","'.pSQL($id_wariant).'","'.pSQL($id_category).'")';
        
        return DB::getInstance()->Execute($sql, 1, 0);
    }
    public static function aktualizujKategorieWariant($id_product, $id_wariant, $id_category) {
        $sql = 'UPDATE '._DB_PREFIX_.'product_wariantowosc_kategorie SET id_product = "'.pSQL($id_product).'", id_wariant = "'.pSQL($id_wariant).'",id_category = "'.pSQL($id_category).'" WHERE id_product = "'.$id_product.'" AND id_wariant = "'.$id_wariant.'"';
        
        return DB::getInstance()->Execute($sql, 1, 0);
    }
    public static function usunKategorieWariant($id_product, $id_wariant) {
        $sql = 'DELETE FROM '._DB_PREFIX_.'product_wariantowosc_kategorie WHERE id_wariant="'.pSQL($id_wariant).'" AND id_product="'.pSQL($id_product).'"';
        
        return DB::getInstance()->Execute($sql, 1, 0);
    }
    public static function pobierzKategorieWariant($id_product, $id_wariant) {
            $sql = 'SELECT id_category FROM '._DB_PREFIX_.'product_wariantowosc_kategorie WHERE id_product = "'.pSQL($id_product).'" AND id_wariant = "'.pSQL($id_wariant).'"';
        
        
        foreach (DB::getInstance()->ExecuteS($sql, 1, 0) as $result) {
            return $result['id_category'];
        }
    }
}