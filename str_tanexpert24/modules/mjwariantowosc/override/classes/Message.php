<?php
/**
 * Main override class of module mjwariantowosc
 * @author Michał Jendraszczyk
 * @copyright (c) 2020, mages.pl
 * @license http://mages.pl mages.pl
 */

class Message extends MessageCore
{

    public static function getMessagesByOrderId($id_order, $private = false, Context $context = null)
    {
        if (!Validate::isBool($private)) {
            die(Tools::displayError());
        }

        if (!$context) {
            $context = Context::getContext();
        }

        $sql = 'SELECT ct.*, cm.*, cl.name subject, e.firstname AS efirstname, e.lastname AS elastname, 
     c.lastname AS clastname, c.firstname AS cfirstname 
     FROM ' . _DB_PREFIX_ . 'customer_thread ct 
     LEFT JOIN ' . _DB_PREFIX_ . 'customer_message cm ON (ct.id_customer_thread = cm.id_customer_thread) 
     LEFT JOIN ' . _DB_PREFIX_ . 'contact_lang cl 
     ON (cl.id_contact = ct.id_contact AND cl.id_lang = ' . (int) Context::getContext()->language->id . ') 
     LEFT JOIN ' . _DB_PREFIX_ . 'employee e 
     ON e.id_employee = cm.id_employee 
     LEFT JOIN ' . _DB_PREFIX_ . 'customer c 
     ON (IFNULL(ct.id_customer, ct.email) = IFNULL(c.id_customer, c.email)) 
     WHERE ct.id_order = ' . (int) $id_order . ' ORDER BY cm.date_add DESC';

        return Db::getInstance()->executeS($sql);
    }
}
